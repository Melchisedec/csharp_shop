﻿namespace ConsignmentShopUI
{
    partial class ConsignmentShop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headerText = new System.Windows.Forms.Label();
            this.ItemslistBox = new System.Windows.Forms.ListBox();
            this.ItemsListBoxlabel = new System.Windows.Forms.Label();
            this.addToCartButton = new System.Windows.Forms.Button();
            this.CartListBoxlabel = new System.Windows.Forms.Label();
            this.CartItemslistBox = new System.Windows.Forms.ListBox();
            this.MakePurchaseButton = new System.Windows.Forms.Button();
            this.VendorListBoxLabel = new System.Windows.Forms.Label();
            this.VendorListBox = new System.Windows.Forms.ListBox();
            this.StoreProfitLabel = new System.Windows.Forms.Label();
            this.StoreProfitValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // headerText
            // 
            this.headerText.AutoSize = true;
            this.headerText.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerText.Location = new System.Drawing.Point(19, 25);
            this.headerText.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.headerText.Name = "headerText";
            this.headerText.Size = new System.Drawing.Size(270, 32);
            this.headerText.TabIndex = 0;
            this.headerText.Text = "Consignment Shop App";
            // 
            // ItemslistBox
            // 
            this.ItemslistBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemslistBox.FormattingEnabled = true;
            this.ItemslistBox.ItemHeight = 18;
            this.ItemslistBox.Location = new System.Drawing.Point(22, 74);
            this.ItemslistBox.Name = "ItemslistBox";
            this.ItemslistBox.Size = new System.Drawing.Size(211, 184);
            this.ItemslistBox.TabIndex = 1;
            // 
            // ItemsListBoxlabel
            // 
            this.ItemsListBoxlabel.AutoSize = true;
            this.ItemsListBoxlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsListBoxlabel.Location = new System.Drawing.Point(22, 55);
            this.ItemsListBoxlabel.Name = "ItemsListBoxlabel";
            this.ItemsListBoxlabel.Size = new System.Drawing.Size(75, 16);
            this.ItemsListBoxlabel.TabIndex = 2;
            this.ItemsListBoxlabel.Text = "Store items";
            this.ItemsListBoxlabel.Click += new System.EventHandler(this.ItemsListBoxlabel_Click);
            // 
            // addToCartButton
            // 
            this.addToCartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addToCartButton.Location = new System.Drawing.Point(260, 131);
            this.addToCartButton.Name = "addToCartButton";
            this.addToCartButton.Size = new System.Drawing.Size(98, 23);
            this.addToCartButton.TabIndex = 3;
            this.addToCartButton.Text = "Add to cart ->";
            this.addToCartButton.UseVisualStyleBackColor = true;
            this.addToCartButton.Click += new System.EventHandler(this.addToCartButton_Click);
            // 
            // CartListBoxlabel
            // 
            this.CartListBoxlabel.AutoSize = true;
            this.CartListBoxlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CartListBoxlabel.Location = new System.Drawing.Point(380, 55);
            this.CartListBoxlabel.Name = "CartListBoxlabel";
            this.CartListBoxlabel.Size = new System.Drawing.Size(67, 16);
            this.CartListBoxlabel.TabIndex = 5;
            this.CartListBoxlabel.Text = "Cart items";
            // 
            // CartItemslistBox
            // 
            this.CartItemslistBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CartItemslistBox.FormattingEnabled = true;
            this.CartItemslistBox.ItemHeight = 18;
            this.CartItemslistBox.Location = new System.Drawing.Point(380, 74);
            this.CartItemslistBox.Name = "CartItemslistBox";
            this.CartItemslistBox.Size = new System.Drawing.Size(211, 184);
            this.CartItemslistBox.TabIndex = 4;
            this.CartItemslistBox.SelectedIndexChanged += new System.EventHandler(this.CartItemslistBox_SelectedIndexChanged);
            // 
            // MakePurchaseButton
            // 
            this.MakePurchaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MakePurchaseButton.Location = new System.Drawing.Point(493, 294);
            this.MakePurchaseButton.Name = "MakePurchaseButton";
            this.MakePurchaseButton.Size = new System.Drawing.Size(98, 23);
            this.MakePurchaseButton.TabIndex = 6;
            this.MakePurchaseButton.Text = "Purchase";
            this.MakePurchaseButton.UseVisualStyleBackColor = true;
            this.MakePurchaseButton.Click += new System.EventHandler(this.MakePurchaseButton_Click);
            // 
            // VendorListBoxLabel
            // 
            this.VendorListBoxLabel.AutoSize = true;
            this.VendorListBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VendorListBoxLabel.Location = new System.Drawing.Point(12, 301);
            this.VendorListBoxLabel.Name = "VendorListBoxLabel";
            this.VendorListBoxLabel.Size = new System.Drawing.Size(59, 16);
            this.VendorListBoxLabel.TabIndex = 8;
            this.VendorListBoxLabel.Text = "Vendors";
            // 
            // VendorListBox
            // 
            this.VendorListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VendorListBox.FormattingEnabled = true;
            this.VendorListBox.ItemHeight = 18;
            this.VendorListBox.Location = new System.Drawing.Point(12, 320);
            this.VendorListBox.Name = "VendorListBox";
            this.VendorListBox.Size = new System.Drawing.Size(211, 184);
            this.VendorListBox.TabIndex = 7;
            this.VendorListBox.SelectedIndexChanged += new System.EventHandler(this.VendorListBox_SelectedIndexChanged);
            // 
            // StoreProfitLabel
            // 
            this.StoreProfitLabel.AutoSize = true;
            this.StoreProfitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StoreProfitLabel.Location = new System.Drawing.Point(344, 337);
            this.StoreProfitLabel.Name = "StoreProfitLabel";
            this.StoreProfitLabel.Size = new System.Drawing.Size(93, 16);
            this.StoreProfitLabel.TabIndex = 9;
            this.StoreProfitLabel.Text = "Store Profit; ";
            // 
            // StoreProfitValue
            // 
            this.StoreProfitValue.AutoSize = true;
            this.StoreProfitValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StoreProfitValue.Location = new System.Drawing.Point(432, 337);
            this.StoreProfitValue.Name = "StoreProfitValue";
            this.StoreProfitValue.Size = new System.Drawing.Size(44, 16);
            this.StoreProfitValue.TabIndex = 10;
            this.StoreProfitValue.Text = "$0.00";
            this.StoreProfitValue.Click += new System.EventHandler(this.label1_Click);
            // 
            // ConsignmentShop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 516);
            this.Controls.Add(this.StoreProfitValue);
            this.Controls.Add(this.StoreProfitLabel);
            this.Controls.Add(this.VendorListBoxLabel);
            this.Controls.Add(this.VendorListBox);
            this.Controls.Add(this.MakePurchaseButton);
            this.Controls.Add(this.CartListBoxlabel);
            this.Controls.Add(this.CartItemslistBox);
            this.Controls.Add(this.addToCartButton);
            this.Controls.Add(this.ItemsListBoxlabel);
            this.Controls.Add(this.ItemslistBox);
            this.Controls.Add(this.headerText);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.Name = "ConsignmentShop";
            this.Text = "Consignment Shop App";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label headerText;
        private System.Windows.Forms.ListBox ItemslistBox;
        private System.Windows.Forms.Label ItemsListBoxlabel;
        private System.Windows.Forms.Button addToCartButton;
        private System.Windows.Forms.Label CartListBoxlabel;
        private System.Windows.Forms.ListBox CartItemslistBox;
        private System.Windows.Forms.Button MakePurchaseButton;
        private System.Windows.Forms.Label VendorListBoxLabel;
        private System.Windows.Forms.ListBox VendorListBox;
        private System.Windows.Forms.Label StoreProfitLabel;
        private System.Windows.Forms.Label StoreProfitValue;
    }
}

