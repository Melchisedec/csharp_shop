﻿using ConsignmntClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsignmentShopUI
{
    public partial class ConsignmentShop : Form
    {
        private Store store = new Store();
        private List<Item> shoppingCartData = new List<Item>();
        BindingSource itemsBinding = new BindingSource();
        BindingSource cartBinding = new BindingSource();
        BindingSource vendorsBinding = new BindingSource();
        private decimal storeProfit = 0;


        public ConsignmentShop()
        {
            InitializeComponent();
            setUpData();
            itemsBinding.DataSource = store.Items.Where(x => x.Sold == false).ToList();
            ItemslistBox.DataSource = itemsBinding;
            ItemslistBox.DisplayMember = "Display";
            ItemslistBox.ValueMember = "Display";

            cartBinding.DataSource = shoppingCartData;
            CartItemslistBox.DataSource = cartBinding;
            CartItemslistBox.DisplayMember = "Display";
            CartItemslistBox.ValueMember = "Display";

            vendorsBinding.DataSource = store.Vendors;
            VendorListBox.DataSource = vendorsBinding;
            VendorListBox.DisplayMember = "Display";
            VendorListBox.ValueMember = "Display";



        }

        private void ItemsListBoxlabel_Click(object sender, EventArgs e)
        {

        }



        private void setUpData()
        {
            /*
            Vendor demoVendor = new Vendor();
            demoVendor.FirstName = "King";
            demoVendor.LastName = "Isaac";
            demoVendor.Commision = .12;
            store.Vendors.Add(demoVendor);

            demoVendor = new Vendor();
            demoVendor.FirstName = "Mwine";
            demoVendor.LastName = "Afrod";
            demoVendor.Commision = .2;
            store.Vendors.Add(demoVendor);
            */

            store.Vendors.Add(new Vendor { FirstName = "Kyambadde", LastName = "Dickson" });
            store.Vendors.Add(new Vendor { FirstName = "King", LastName = "Isaac", Commision = .12 });
            store.Vendors.Add(new Vendor { FirstName = "Mwine", LastName = "Afrod", Commision = .2 });

            store.Items.Add(new Item
            {
                Title = "Moby Dick",
                Description = "A book about a whale",
                Price = 4.50M,
                //Sold = true,
                //PaymentDistributed = true,
                Owner = store.Vendors[0]
            });

            store.Items.Add(new Item
            {
                Title = "A tale of two cities",
                Description = "Mr bean takes a holday",
                Price = 20.80M,
                //Sold = true,
                //PaymentDistributed = true,
                Owner = store.Vendors[2]
            });

            store.Items.Add(new Item
            {
                Title = "Harry Porter",
                Description = "A book about a boy",
                Price = 12.10M,
                //Sold = true,
                //PaymentDistributed = true,
                Owner = store.Vendors[1]
            });

            store.Items.Add(new Item
            {
                Title = "Jane Eyre",
                Description = "A book about a Girl",
                Price = 3.1M,
                //Sold = true,
                //PaymentDistributed = true,
                Owner = store.Vendors[1]
            });

            store.Name = "Winna Classic";
        }

        private void CartItemslistBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addToCartButton_Click(object sender, EventArgs e)
        {
            
            // figure out what has been selected in the Items list
            // Copy it to the Shopping cart list
            // Do we remove the item from the list? - no

            Item selectedItem = (Item) ItemslistBox.SelectedItem;
            //MessageBox.Show(selectedItem.Title);
            shoppingCartData.Add(selectedItem);
            cartBinding.ResetBindings(false);

        }

        private void MakePurchaseButton_Click(object sender, EventArgs e)
        {
            // Mark Item in cart as sold
            // clear the cart
            foreach (Item item in shoppingCartData)
            {
                item.Sold = true;
                item.Owner.PaymentDue += (decimal)item.Owner.Commision * item.Price;
                storeProfit += (1-(decimal)item.Owner.Commision) * item.Price;
            }

            shoppingCartData.Clear();
            itemsBinding.DataSource = store.Items.Where(x => x.Sold == false).ToList();
            StoreProfitValue.Text = string.Format("${0}",storeProfit);
            cartBinding.ResetBindings(false);
            itemsBinding.ResetBindings(false);
            vendorsBinding.ResetBindings(false);
        }

        private void VendorListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
